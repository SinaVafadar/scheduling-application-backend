from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

from accounting.views import SignUpAPIView, ChangePasswordAPIView

app_name = "accounting"

urlpatterns = [
    path("login", obtain_jwt_token),
    path("verify_token", verify_jwt_token),
    path("refresh_token", refresh_jwt_token),
    path("signup", SignUpAPIView.as_view()),
    path("change_password", ChangePasswordAPIView.as_view()),
]
