from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "password",
            "email"
        ]

        extra_kwargs = {
            'password': {'write_only': True},
        }

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class ChangePasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField(max_length=200)
    new_password = serializers.CharField(max_length=200)

    def validate(self, attrs):
        user = self.context.get('user')
        if not user.check_password(attrs.get('current_password')):
            raise ValidationError("Incorrect Password")
        return attrs
