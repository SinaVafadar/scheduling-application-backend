from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounting.serializers import UserSerializer, ChangePasswordSerializer


class SignUpAPIView(CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class ChangePasswordAPIView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ChangePasswordSerializer

    def post(self, request):
        ser = self.serializer_class(data=request.data, context={'user': request.user})
        ser.is_valid(raise_exception=True)
        request.user.set_password(ser.validated_data.get('new_password'))
        request.user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
