from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from polls.models import Session, TimePoll


class SessionSerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Session
        fields = [
            "id",
            "start",
            "end",
            "votes"
        ]

    def validate(self, attrs):
        if attrs.get('start') >= attrs.get('end'):
            raise ValidationError("Invalid start and end time!")
        return attrs

    def get_votes(self, obj):
        votes = []
        if obj.time_poll.is_hidden and self.context.get('request').user != obj.time_poll.creator:
            for vote in obj.votes.filter(voter=self.context.get('request').user):
                votes.append(vote.voter.username)
        else:
            for vote in obj.votes.all():
                votes.append(vote.voter.username)
        return votes


class TimePollSerializer(serializers.ModelSerializer):
    permitted_voters_emails = serializers.SerializerMethodField(read_only=True)
    permitted_voters = serializers.ListField(write_only=True)
    sessions = SessionSerializer(many=True)

    class Meta:
        model = TimePoll
        fields = [
            "id",
            "title",
            "note",
            "location",
            "physical_location",
            "is_hidden",
            "is_one_vote_participant",
            "is_vote_limited",
            "limit_option_count",
            "time_created",
            "permitted_voters",
            "permitted_voters_emails",
            "sessions",
        ]

        read_only_fields = ("id", "time_created")

    def validate_permitted_voters(self, value):
        res = []
        for email in value:
            qs = User.objects.filter(email=email)
            if not qs.exists():
                raise ValidationError(f"{email} does not exist!")
            res.append(qs.first())

        return res

    def validate(self, attrs):
        if attrs.get('is_vote_limited') and not attrs.get('limit_option_count'):
            raise ValidationError("limit_option_count cannot be null with this combination!")

        if attrs.get('location') is not None and attrs.get('physical_location') is not None:
            raise ValidationError("Both physical_location and location field cannot contain value!")
        return attrs

    def get_permitted_voters_emails(self, obj):
        return [x.email for x in obj.permitted_voters.all()]

    def create(self, validated_data):
        sessions = validated_data.pop('sessions')
        permitted_voters = validated_data.pop('permitted_voters')
        poll = TimePoll.objects.create(**validated_data)
        poll.permitted_voters.set(permitted_voters)
        for session in sessions:
            Session.objects.create(time_poll=poll, **session)
        return poll


class TimePollBriefSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimePoll
        fields = [
            "id",
            "title",
            "time_created"
        ]
