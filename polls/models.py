from django.contrib.auth.models import User
from django.db import models
from location_field.models.plain import PlainLocationField


class TimePoll(models.Model):
    title = models.CharField(max_length=200)
    note = models.TextField(null=True, blank=True)
    location = models.CharField(max_length=50, null=True, blank=True)
    physical_location = PlainLocationField(null=True, blank=True)
    is_hidden = models.BooleanField(default=False)
    is_one_vote_participant = models.BooleanField(default=False)
    is_vote_limited = models.BooleanField(default=False)
    limit_option_count = models.IntegerField(blank=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True)
    permitted_voters = models.ManyToManyField(User, blank=True)
    creator = models.ForeignKey(User, on_delete=models.PROTECT, related_name='time_polls')


class Session(models.Model):
    time_poll = models.ForeignKey(TimePoll, on_delete=models.CASCADE, related_name='sessions')
    start = models.DateTimeField()
    end = models.DateTimeField()
