from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView, CreateAPIView, DestroyAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated

from polls.models import Session, TimePoll
from polls.permissions import (IsTimePollOwnerPermission, IsSessionOwnerPermission)
from polls.serializers import SessionSerializer, TimePollSerializer, TimePollBriefSerializer


class TimePollListCreateAPIView(ListCreateAPIView):
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method.lower() == "get":
            return TimePollBriefSerializer
        return TimePollSerializer

    def get_queryset(self):
        return self.request.user.time_polls.order_by('-time_created')

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class AddSessionAPIView(CreateAPIView):
    permission_classes = [IsAuthenticated, IsTimePollOwnerPermission]
    queryset = Session.objects.all()
    serializer_class = SessionSerializer

    def perform_create(self, serializer):
        serializer.save(time_poll=get_object_or_404(TimePoll, id=self.kwargs.get('time_poll_id')))


class RemoveSessionAPIView(DestroyAPIView):
    permission_classes = [IsAuthenticated, IsSessionOwnerPermission]
    queryset = Session.objects.all()
    lookup_url_kwarg = 'session_id'


class TimePollRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = TimePollSerializer
    queryset = TimePoll.objects.all()
    permission_classes = [IsAuthenticated, IsTimePollOwnerPermission]
    lookup_url_kwarg = 'time_poll_id'
