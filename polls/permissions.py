from rest_framework.permissions import BasePermission, SAFE_METHODS

from polls.models import TimePoll


class IsTimePollOwnerPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        qs = TimePoll.objects.filter(creator=request.user, id=view.kwargs.get('time_poll_id'))
        if not qs.exists():
            return False
        return True

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        if request.user == obj.creator:
            return True
        return False


class IsSessionOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        qs = TimePoll.objects.filter(creator=request.user, id=obj.time_poll.id)
        if not qs.exists():
            return False
        return True
