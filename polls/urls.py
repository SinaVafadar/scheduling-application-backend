from django.urls import path

from polls.views import (TimePollListCreateAPIView, AddSessionAPIView, RemoveSessionAPIView,
                         TimePollRetrieveUpdateDestroyAPIView)

app_name = 'polls'

urlpatterns = [
    path('list_create_time_poll', TimePollListCreateAPIView.as_view()),
    path('add_section/<int:time_poll_id>', AddSessionAPIView.as_view()),
    path('remove_session/<int:session_id>', RemoveSessionAPIView.as_view()),
    path('time_poll_rud/<int:time_poll_id>', TimePollRetrieveUpdateDestroyAPIView.as_view()),
]
