from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from polls.models import TimePoll
from polls.serializers import TimePollBriefSerializer
from votes.models import SessionVote
from votes.serializers import CreateSessionVoteSerializer


class VoteSessionAPIView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = CreateSessionVoteSerializer

    def post(self, request):
        ser = self.serializer_class(data=request.data, context={'user': request.user})
        ser.is_valid(raise_exception=True)
        sessions = ser.validated_data.get('sessions')
        poll = ser.validated_data.get('poll')

        current_votes = SessionVote.objects.filter(voter=request.user, session__time_poll=poll)
        for vote in current_votes:
            if vote.session not in sessions:
                vote.delete()

        current_sessions = [x.session for x in current_votes]
        for session in sessions:
            if session not in current_sessions:
                SessionVote.objects.create(voter=request.user, session=session)
        return Response(status=status.HTTP_204_NO_CONTENT)


class GetMyVotesBriefAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = TimePollBriefSerializer

    def get_queryset(self):
        return TimePoll.objects.filter(permitted_voters=self.request.user).order_by('-time_created')
