from django.urls import path

from votes.views import VoteSessionAPIView, GetMyVotesBriefAPIView

app_name = 'votes'

urlpatterns = [
    path('vote_session', VoteSessionAPIView.as_view()),
    path('get_my_votes_brief', GetMyVotesBriefAPIView.as_view()),
]
