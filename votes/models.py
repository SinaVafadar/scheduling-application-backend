from django.contrib.auth.models import User
from django.db import models

from polls.models import Session


class SessionVote(models.Model):
    voter = models.ForeignKey(User, on_delete=models.PROTECT)
    session = models.ForeignKey(Session, on_delete=models.CASCADE, related_name='votes')
