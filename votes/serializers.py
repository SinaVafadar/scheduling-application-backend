from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from accounting.serializers import UserSerializer
from polls.models import Session, TimePoll
from polls.serializers import SessionSerializer
from votes.models import SessionVote


class SessionVoteSerializer(serializers.ModelSerializer):
    session = SessionSerializer()
    voter = UserSerializer()

    class Meta:
        model = SessionVote
        fields = [
            "id",
            "voter",
            "session"
        ]


class CreateSessionVoteSerializer(serializers.Serializer):
    sessions = serializers.PrimaryKeyRelatedField(queryset=Session.objects.all(), many=True)
    poll = serializers.PrimaryKeyRelatedField(queryset=TimePoll.objects.all())

    def validate(self, attrs):
        sessions = attrs.get('sessions')
        poll = attrs.get('poll')

        for session in sessions:
            if session.time_poll != poll:
                raise ValidationError("Invalid Session")

        if self.context.get('user') not in poll.permitted_voters.all():
            raise ValidationError("You do not have access to this poll!")
        if poll.is_one_vote_participant and len(sessions) > 1:
            raise ValidationError("Only one vote is allowed!")
        if poll.is_vote_limited:
            for session in sessions:
                if SessionVote.objects.filter(session=session).count() >= poll.limit_option_count:
                    raise ValidationError("Session Vote Limit Exceeded!")

        return attrs
